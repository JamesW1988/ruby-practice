print "Enter a number: "
number_one = Integer(gets.chomp)

print "Enter an operator to perform: "
operator = gets.chomp

print "Enter another number: "
number_two = Integer(gets.chomp) 

if operator == "*" then
    print number_one * number_two 

elsif operator == "+" then
    print number_one + number_two

elsif operator == "-" then
    print number_one - number_two 

else
    print number_one / number_two 
end 